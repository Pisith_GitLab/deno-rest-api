import { Response, RouteParams } from "https://deno.land/x/oak/mod.ts";
// import { Response,  } from 'https://deno.land/x/oak/mod.ts';
import { getStudent } from './services.ts';
// import Student from './IStudent.ts';
// import { repeat } from "../../../../../Users/Pisith/AppData/Local/deno/deps/https/deno.land/e4006c6840d03176e465fb5223a8fe47d4acf6658e8759a78e8e9c5eab5270eb.ts";

export default async ({
    params,
    response
}: {
    params: RouteParams,
    response: Response
}) => {
    const studentId = params.roll_no;
    if (!studentId) {
        response.status = 400
        response.body = { msg: "Invalid student id" };
        return;
    }

    const foundStudent = await getStudent(studentId);
    if (!foundStudent) {
        response.status = 404;
        response.body = { msg: `Student with ID ${studentId} not found` };
        return;
    }

    response.status = 200
    response.body = foundStudent;
}

