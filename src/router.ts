import { Router } from 'https://deno.land/x/oak/mod.ts';
import getStudents from './getStudents.ts';
import getStudent from './getStudent.ts';

const router = new Router();

router
    .get('/students', getStudents)
    .get('/student/:roll_no', getStudent);

export default router;