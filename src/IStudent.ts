export default interface Student {
    roll_no: string;
    name: string;
    grade: string;
}