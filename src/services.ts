import { Response } from 'https://deno.land/x/oak/mod.ts';
import Student from './IStudent.ts';

let studentData: Array<Student> = [{
    roll_no: "1",
    name: "John",
    grade: "A",
  },{
    roll_no: "2",
    name: "Doe",
    grade: "B",
  },{
    roll_no: "3",
    name: "Nick",
    grade: "B",
  }]

export const getStudents = async(): Promise<Student[]> => {
    const data = studentData;
    return data;
}

export const getStudent = async(studentId: string): Promise<Student | undefined> => {
  const students = studentData;
  return studentData.find(({ roll_no }) => roll_no === studentId);
}