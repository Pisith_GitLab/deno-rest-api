import { Application } from 'https://deno.land/x/oak/mod.ts';
import { HOST, PORT } from './src/config.ts';
import router from './src/router.ts';
import notFound from './src/404.ts';
import middlewareError from './src/middlewareError.ts';

const app = new Application();
app.use(router.routes());
app.use(router.allowedMethods());
app.use(notFound);
app.use(middlewareError);

console.log(`Application is runnin on port ${PORT}`);
await app.listen(`${HOST}:${PORT}`);