# Deno.js
A simple REST API using deno.js.

Check this out => [deno](https://deno.land/)

## Installation
+ Using PowerShell in windows
```bash
iwr https://deno.land/x/install/install.ps1 -useb | iex
```

+ Using Shell (macOS/Linux)
```bash
curl -fsSL https://deno.land/x/install/install.sh | sh
```

## Run
```bash
deno run index.ts
```